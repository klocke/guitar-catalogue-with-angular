import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	template: `
		<app-container>
			<header>
				<h1>My Guitar catalogue</h1>
			</header>
		</app-container>

		<router-outlet></router-outlet>
	`
})
export class AppComponent {
}

