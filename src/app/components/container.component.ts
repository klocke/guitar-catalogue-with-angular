import { Component } from "@angular/core";

@Component({
	selector: 'app-container',
	template: `
		<div class="container">
			<ng-content></ng-content>
		</div>
	`,
	styles:[
		`.container {
			max-width: 76rem;
			width: 100%;
			padding: 0 1.5rem;
			margin: 0 auto;
		}`
	]
})
export class ContainerComponent {}
