import { Component } from "@angular/core";
import { UserService } from "../../services/user.service";
import { Router } from "@angular/router";

@Component({
	selector: 'app-login-page',
	templateUrl: './login.page.html'
})
export class LoginPage {
	constructor(
		private readonly router: Router,
		private readonly userService: UserService
	) {
	}

	get attempting(): boolean {
		return this.userService.attempting;
	}

	onLoginClick(): void {
		this.userService.authenticate('luke-skywalker', async () => {
			// Success!
			await this.router.navigate(['catalogue'])
		})
	}

}
